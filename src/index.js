import Axeptio from './axeptio';
import GoogleAnalytics from './google-analytics';
import GoogleTagManager from './google-tag-manager';
import GoogleAnalyticsFactory from './google-analytics-factory';



export {
    Axeptio,
    GoogleAnalytics,
    GoogleTagManager,
    GoogleAnalyticsFactory
}
