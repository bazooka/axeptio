class Axeptio {

    constructor(settings) {
        this.strategies = [];

        /**
         * @type {string} clientId              ObjectID corresponding to the project object
         * @type {boolean} userCookiesSecure     Weather or not the cookie holding choices is HTTPS only
         * @type {string} cookiesVersion        String identifier of the version of Cookie configuration that should be loaded. If this parameter is omitted, then it's the "pages" property in the configuration that gets parsed in case of multiple cookies configurations.
         */
        this.settings = {
            userCookiesSecure: false,
            ...settings
        }

        this.init = this.init.bind(this);
        this.add = this.add.bind(this);
        this.loadScript = this.loadScript.bind(this);
        this.loadStrategies = this.loadStrategies.bind(this);
        this.onCookiesComplete = this.onCookiesComplete.bind(this);
    }

    add(strategy){
        this.strategies.push(strategy);
    }

    init(){
        window.axeptioSettings = this.settings;

        this.loadScript();
        this.loadStrategies();
    }

    /**
     * Load Axeptio script.
     */
    loadScript(){
        const el = document.createElement('script')
        el.setAttribute('async', true)
        el.setAttribute('src', '//static.axept.io/sdk.js')
        document.body.appendChild(el)
    }

    loadStrategies(){

        void 0 === window._axcb && (window._axcb = [])

        window._axcb.push((axeptio) => {
            /**
             * On user consent.
             */
            axeptio.on('cookies:complete', this.onCookiesComplete)
        })
    }

    onCookiesComplete(choices){

        this.strategies.forEach( (strategy) => {
            if (!strategy.id) {
                return
            }

            if (choices[strategy.name]) {
                strategy.enableTracking()
                strategy.init()
            } else {
                strategy.disableTracking()
            }
        } )
    }
}

export default Axeptio;
