class GoogleTagManager {

    constructor(propertyId) {
        this.id = propertyId;
        this.name = 'google_tag_manager';
        this.src = `//www.googletagmanager.com/gtag/js?id=${this.id}`;

        this.gtag = this.gtag.bind(this);
        this.loadScript = this.loadScript.bind(this);
        this.init = this.init.bind(this);
        this.enableTracking = this.enableTracking.bind(this);
        this.disableTracking = this.disableTracking.bind(this);
    }


    /**
     * Async Load Google Tag Manager script.
     */
    loadScript() {
        window.dataLayer = window.dataLayer || []

        const el = document.createElement('script')
        el.setAttribute('async', true)
        el.setAttribute('src', this.src)
        document.body.appendChild(el)
    }

    gtag() {
        window.dataLayer.push(arguments)
    }

    /**
     * Init Google Analytics
     */
    init() {
        const gaScript = document.querySelector(`[src="${this.src}"]`);
        if (!gaScript) {
            this.loadScript()
        }
        // We decided to send a manual pageview to make sure
        // we do not lose the first visit tracking information
        this.gtag('js', new Date())
        this.gtag('consent', 'default', {
            ad_storage: 'denied',
            analytics_storage: 'denied'
        })
        this.gtag('config', this.id, {anonymize_ip: true})
    }

    enableTracking() {
        window[`ga-disable-${this.id}`] = false
        this.gtag('consent', 'update', {
            analytics_storage: 'granted'
        })
    }

    disableTracking() {
        window[`ga-disable-${this.id}`] = true
        this.gtag('consent', 'update', {
            analytics_storage: 'denied'
        })
    }
}

export default GoogleTagManager;
