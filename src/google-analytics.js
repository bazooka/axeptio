class GoogleAnalytics {

    constructor(propertyID) {
        this.id = propertyID;
        this.name = 'google_analytics';
        this.src = '//www.google-analytics.com/analytics.js';

        this.loadScript = this.loadScript.bind(this);
        this.init = this.init.bind(this);
        this.enableTracking = this.enableTracking.bind(this);
        this.disableTracking = this.disableTracking.bind(this);
    }

    /**
     * Async Load Google Analytics script.
     */
    loadScript() {
        window.GoogleAnalyticsObject = 'ga'
        window.ga = window.ga || function () {
            (window.ga.q = window.ga.q || []).push(arguments)
        }
        window.ga.l = 1 * new Date()
        const el = document.createElement('script')
        el.setAttribute('async', true)
        el.setAttribute('src', this.src)
        document.body.appendChild(el)
    }

    /**
     * Init Google Analytics
     */
    init() {
        const gaScript = document.querySelector(`[src="${this.src}"]`);
        if (!gaScript) {
            this.loadScript()
        }
        // We decided to send a manual pageview to make sure
        // we do not lose the first visit tracking information
        window.ga('create', this.id, 'auto')
        window.ga('send', 'pageview', {
            anonymizeIp: true
        })
    }

    enableTracking() {
        window[`ga-disable-${this.id}`] = false
    }

    disableTracking() {
        window[`ga-disable-${this.id}`] = true
    }
}

export default GoogleAnalytics;
