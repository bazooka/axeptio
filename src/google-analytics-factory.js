import GoogleAnalytics from "./google-analytics";
import GoogleTagManager from "./google-tag-manager";

class GoogleAnalyticsFactory {

    createGoogleAnalytics(propertyID) {
        let ga = new GoogleAnalytics(propertyID);

        if (propertyID.startsWith('G-')) {
            ga = new GoogleTagManager(propertyID);
        }

        // Force Google Tag Manager to be Google Analytics provider (for Google Analytics 4 for example).
        ga.name = 'google_analytics';

        return ga;
    }
}

export default GoogleAnalyticsFactory;
