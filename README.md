Manage tracking scripts with Axeptio.

## 📦 How to install this project ?
```bash
npm install @fantassin/axeptio
```

## ⚡ How it works ?

```js
import { Axeptio, GoogleAnalytics, GoogleTagManager } from '@fantassin/axeptio';

const settings = {
    clientId: 'axeptioId'
};

const axeptio = new Axeptio( settings );

// Add tracking strategies.
axeptio.add( new GoogleAnalytics( 'UA-propertyId' ) );
axeptio.add( new GoogleTagManager( 'GTM-propertyId' ) );

// Load Axeptio script.
axeptio.init()

```

In the `settings` you can use all the Axeption SDK params, for more information see the documention https://developers.axeptio.eu/sdk/integration-du-sdk.

—

If you don't know which object to create you can use `GoogleAnalyticsFactory`, for example : 

```js
import { GoogleAnalyticsFactory } from '@fantassin/axeptio';

const factory = GoogleAnalyticsFactory();

// Create a Google Analytics object.
axeptio.add( factory.createGoogleAnalytics( 'UA-propertyId' ) );
// Create a Google Analytics 4 object.
axeptio.add( factory.createGoogleAnalytics( 'G-propertyId' ) ); 

```
